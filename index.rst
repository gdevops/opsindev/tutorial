.. DevOps tutorial documentation master file, created by
   sphinx-quickstart on Sat Mar 24 14:41:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/tutorial/rss.xml>`_


.. _devops_tutorial:
.. _tuto_devops:

====================
Opsindev tutorial
====================

- https://opsindev.news/

.. figure:: images/DevSecOps-security.png
   :align: center

Devops Definition
==================

- https://en.wikipedia.org/wiki/DevOps
- https://forum.compagnons-devops.fr/
- https://lydra.fr/radio-devops/


DevOps (a clipped compound of "development" and "operations") is a
software engineering culture and practice that aims at unifying
software development (Dev) and software operation (Ops).

The main characteristic of the DevOps movement is to strongly advocate
automation and monitoring at all steps of software construction, from
integration, testing, releasing to deployment and infrastructure
management.

DevOps aims at shorter development cycles, increased deployment
frequency, more dependable releases, in close alignment with
business objectives.


.. contents::
   :depth: 5


.. figure:: _static/DevSecOps-security.png
   :align: center

   SecDevOps

People
==========

.. toctree::
   :maxdepth: 3

   people/people



dagger
==========

.. toctree::
   :maxdepth: 3

   dagger/dagger



o11y
==========

.. toctree::
   :maxdepth: 3

   o11y/o11y



