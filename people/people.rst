.. index::
   pair: DevOps ; People

.. _devops_people:

====================
DevOps People
====================

- https://github.com/stephrobert/awesome-french-devops#DescomptesTwitters


People
========

.. toctree::
   :maxdepth: 3

   stephane_robert/stephane_robert
   xavki/xavki
   michael_friedrich/michael_friedrich

https://mobile.x.com/aurelievache
==========================================

- https://mobile.x.com/aurelievache
- https://github.com/scraly


https://x.com/damyr_fr
==============================

- https://x.com/damyr_fr

https://x.com/dadideo
================================

- https://x.com/dadideo

https://x.com/idriss_neumann
=====================================

- https://x.com/idriss_neumann

https://x.com/ImraneSubstack
====================================

- https://x.com/ImraneSubstack

https://x.com/ajuliettedev
====================================

- https://x.com/ajuliettedev

https://mobile.x.com/katia_tal
================================================

- https://mobile.x.com/katia_tal

https://x.com/_mcorbin
===============================

- https://x.com/_mcorbin
