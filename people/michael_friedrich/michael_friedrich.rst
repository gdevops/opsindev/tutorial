.. index::
   pair: People; Michael Friedrich
   ! Michael Friedrich

.. _michael_friedrich:

========================
**Michael Friedrich**
========================

- https://crashloop.social/@dnsmichi (mastodon)
- https://opsindev.news/
- https://gitlab.com/dnsmichi
- https://dnsmichi.at/
- https://dnsmichi.at/rss/
- https://about.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/


.. figure:: images/mastodon__michael_friedrich.png
   :align: center

   https://crashloop.social/@dnsmichi , @dnsmichi@crashloop.social

How to pronounce
====================

Pronounced as: "Michi" is the lovely version of "Michael" in German.

It is hard to pronounce in English [mee ch ee], "Friedrich" [free dree ch|ck]
is a tongue breaker too.

Just go with the English pronunciation of "Michael" [mai - kl] :)
